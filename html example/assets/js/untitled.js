jQuery(document).ready(function() {
	console.log('JQury lives!');
	
        jQuery.widget( "custom.catcomplete", $.ui.autocomplete, {
      _create: function() {
        this._super();
        this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
      },
      _renderMenu: function( ul, items ) {
        var that = this,
          currentCategory = "";
        jQuery.each( items, function( index, item ) {
          var li;
          if ( item.category != currentCategory ) {
            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
            currentCategory = item.category;
          }
          li = that._renderItemData( ul, item );
          if ( item.category ) {
            li.attr( "aria-label", item.category + " : " + item.label );
          }
        });
      }
    });
    var demodata = [
        {label: "Email", category: "Popular Digital"},
{label: "PPC", category: "Popular Digital"},
{label: "Brand SEO", category: "Popular Digital"},
{label: "Topic SEO", category: "Popular Digital"},
{label: "Article SEO", category: "Popular Digital"},
{label: "Backlink", category: "Other Digital"},
{label: "Display ad", category: "Other Digital"},
{label: "Affiliate Marketing", category: "Other Digital"},
{label: "Video ad", category: "Other Digital"},
{label: "Sponsorship", category: "Other Digital"},
{label: "Partnership", category: "Other Digital"},
{label: "Audio/radio ad", category: "Other Digital"},
{label: "Social media", category: "Popular Digital"},
{label: "Rating/review site", category: "Popular Digital"},
{label: "Directory/map site", category: "Popular Digital"},
{label: "Coupon/promotion", category: "Other Digital"},
{label: "Local TV", category: "Offline Marketing"},
{label: "National TV", category: "Offline Marketing"},
{label: "Local Radio", category: "Offline Marketing"},
{label: "National Radio", category: "Offline Marketing"},
{label: "Local Print", category: "Offline Marketing"},
{label: "National Print", category: "Offline Marketing"},
{label: "Billboard/Venue", category: "Offline Marketing"},
{label: "Tradeshows", category: "Offline Marketing"},
{label: "Events", category: "Offline Marketing"},
{label: "Sponsorships", category: "Offline Marketing"}
    ];    
var data=[
    {label: "PPC", category: "My Channels"},
{label: "Article SEO", category: "My Channels"},
{label: "Backlink", category: "My Channels"},
{label: "Social media", category: "My Channels"},
{label: "Directory/map site", category: "My Channels"},
{label: "Local Radio", category: "My Channels"},
{label: "Rating/review site", category: "Popular Digital"},
{label: "Email", category: "Popular Digital"},
{label: "Brand SEO", category: "Popular Digital"},
{label: "Topic SEO", category: "Popular Digital"},
{label: "Display ad", category: "Other Digital"},
{label: "Affiliate Marketing", category: "Other Digital"},
{label: "Video ad", category: "Other Digital"},
{label: "Sponsorship", category: "Other Digital"},
{label: "Partnership", category: "Other Digital"},
{label: "Audio/radio ad", category: "Other Digital"},
{label: "Coupon/promotion", category: "Other Digital"},
{label: "Local TV", category: "Offline Marketing"},
{label: "National TV", category: "Offline Marketing"},
{label: "National Radio", category: "Offline Marketing"},
{label: "Local Print", category: "Offline Marketing"},
{label: "National Print", category: "Offline Marketing"},
{label: "Billboard/Venue", category: "Offline Marketing"},
{label: "Tradeshows", category: "Offline Marketing"},
{label: "Events", category: "Offline Marketing"},
{label: "Sponsorships", category: "Offline Marketing"}
    ];
 
    jQuery( "#channels" ).catcomplete({
      delay: 0,
      source: demodata
    });
    
    var listOfWpPagesData = [
        {label: "http://example.com/category/blog-on-topic", category: "Post"},
{label: "http://example.com/category/", category: "Category"},
{label: "http://example.com/tag/", category: "Tag"},
{label: "http://example.com/landing-page", category: "Page"},
{label: "http://example.com/product-category/product-1", category: "Product"},
{label: "http://example.com/category-2/blog-on-topic-2", category: "Post"},
{label: "http://example.com/category-2/", category: "Category"},
{label: "http://example.com/tag-2/", category: "Tag"},
{label: "http://example.com/landing-page-2", category: "Page"},
{label: "http://example.com/product-category/product-2", category: "Product"},
{label: "http://example.com/", category: "Home"}
    ];    
 
    jQuery( "#list-of-wp-pages" ).catcomplete({
      delay: 0,
      source: listOfWpPagesData
    });

    
    
        var listOfWpPagesData = [
{label: "Local store 123", category: ""},
{label: "Knowledge Leaders campaign", category: ""},
{label: "Lowest Rates campaign", category: ""},
{label: "Mother's Day 2018", category: ""}
    ];    
 
    jQuery( "#campaigns-list" ).catcomplete({
      delay: 0,
      source: listOfWpPagesData
    });

    
    
    metaIterater=0;
    jQuery('#add-meta').click(function(){
        var metaPlaceholders= ['utm_source','utm_term','utm_content','my_meta'];
        if(metaIterater<4) {
		    jQuery(this).after('<br/><input class="form-control" type="text"         placeholder="'+metaPlaceholders[metaIterater]+'">');
            metaIterater++;
        }
	});
	
	
	jQuery('#ajax-button').click(function(){
		jQuery.ajax('phone-alert.php');
	});
});