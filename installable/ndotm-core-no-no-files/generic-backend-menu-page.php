<?php	
		/**
		* Include the ndotm "DOT" icon as an svg bit stream and create the menu
		* @link https://developer.wordpress.org/reference/functions/add_menu_page/
		* @link https://developer.wordpress.org/reference/functions/add_submenu_page/
		*/
			include NDOTM_HARDWOOD_PLUGIN_DIR.'/img/dot-svg.php';
			if (empty($GLOBALS['admin_page_hooks']['ndotm-admin-menu-slug'])){
				add_menu_page('N.M Options', 'N.M Options', 'manage_options', 'ndotm-admin-menu-slug', 'ndotm_admin_page_func', 'data:image/svg+xml;base64,'.$ndotm_dot_svg);
				add_submenu_page( 'ndotm-admin-menu-slug', 'General', 'General', 'manage_options', 'ndotm-admin-menu-slug', 'ndotm_admin_page_func');
			}
		// END  ndotm "DOT" icon as an svg bit stream. Test: echo 'Here: '.$ndotm_dot_svg.'and <a href=" admin_url().'/admin.php?page=ndotm-admin-menu-slug/">Here</a>';
		
		
		/**
		* Output generic ndotm page content for generic ndotm backend page when plugin is active. DO NOT EDIT, but use hooks/filters.
		*
		* IMPORTANT - DO NOT EDIT THE CONTENT OF /ndotm-core-no-no-files/generic-backend-menu-page.php. THis is a page that is created if this is your first N.M plugin, but skipped if you already have one. It is NOT MEANT TO BE CUSTOMIZED IN EACH PLUGIN, but instead you can use the following hooks and filters.
		*
		* @action ndotm_admin_page_before_action
		* @filter ndotm_admin_page_content_filter
		* @action ndotm_admin_page_after_action
		* @todo Basically everything //Create a menu item and page that shows on the backend when plugin is active. If you are using the Settings API to save data, and need the user to be other than the administrator, will need to modify the permissions via the hook option_page_capability_{$option_group}, where $option_group is the same as option_group in register_setting() . Check out the Settings API.
		*/		
			function ndotm_admin_page_func(){
				do_action('ndotm_admin_page_before_action');
				echo apply_filters('ndotm_admin_page_content_filter','This is where configuration options can go for the general Nineteen.Marketing backend page called "N.M -> General"...<br/><lable>Theme Color</lable><input value="#666666" type="text"/>(this form still needs to be handled...)');
				//NEED to add handling for any form in the above. For now...hardcode:
				do_action('ndotm_admin_page_after_action');
			}
		//END Output generic ndotm page content for generic ndotm backend page. TEST: echo '<a href=" admin_url().'/admin.php?page=ndotm-admin-menu-slug/">Here</a>';
?>