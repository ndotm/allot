USING PLUGIN TEMPLATE
	To use any file in this tempalte repo, you will wanna replace "hardwood" with your current project wood, as well as "Hardwood" and "HARDWOOD", including in file names. Also find "Adjective" as that is an indication of descriptive text that needs changing. Remember to keep it light, deleting any code you don't need (ie. not every plugin will need its own backend page or menu item.


USING POST TYPES
	To use the post type templates, you will search for "bookshelf" and "bookshelves" and replace them, including in file names. "bookshelf" will be a single post of our new post type and "bookshelves" a plural/list like an archive. You'll also need to do "Bookshelf" and "Bookshelves", but not "BOOKSHELVES".
	
	
USISING ROLES
	To use roles, replace "finish", "finishes", "Finish", "Finishes" with yoru user meta. Eventually there will be more here about actually establishing user roles for the plug

desk
bookshelf
crowne moulding
chair

furniture
engraving
stained
woodgrain
handcrafted
