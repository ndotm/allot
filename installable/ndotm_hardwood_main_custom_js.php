<?php header("Content-type: text/javascript; charset: UTF-8"); ?>
//NO hardcoded text or urls - pass with php or grab from html

//write tasks or requirements in terms of tests
//leave tests there so that the more complex script gets you still test all needs and cases/scenarios
//write test->write code->run test-> until passes->write next test

//make an object not just a var, to store several factors (price, month price,  year price, etc.)
objectvar={"red":"one","phone":"droid"}
console.log ('objectvar', objectvar.red);

arrayvar=["beats","bears","battlestartgwacktica"]
console.log ('arrayvar', arrayvar);

//test JQuery is up
jQuery(document).ready(function() {
	console.log('JQury lives!');
	
	//test you can do crap on clicking thing
	//in console, run
	//clear();
	//$('#thing').trigger('click');
	jQuery('#thing').click(function(){
		$('#thing').doCrap();
		console.log('didcrap');
	});
	
	
	jQuery('#ajax-button').click(function(){
		jQuery.ajax('phone-alert.php');
	});
});