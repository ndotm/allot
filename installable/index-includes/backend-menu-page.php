<?php
	function ndotm_hardwood_admin_menu_func(){
		/**
		* Include the parent N.M menu item on the Wordpress backend dashboard menu and the generic "N.M -> General" page.
		*
		* IMPORTANT - DO NOT EDIT THE CONTENT OF /ndotm-core-no-no-files/generic-backend-menu-page.php. THis is a page that is created if this is your first N.M plugin, but skipped if you already have one. It is NOT MEANT TO BE CUSTOMIZED IN EACH PLUGIN, but instead you can use the following hooks and filters.
		*
		* @action ndotm_admin_page_before_action
		* @filter ndotm_admin_page_content_filter
		* @action ndotm_admin_page_after_action
		*/
			include NDOTM_HARDWOOD_PLUGIN_DIR.'/ndotm-core-no-no-files/generic-backend-menu-page.php';
		// END  Include the parent N.M menu item and the generic "N.M -> General" page. TEST:  echo '<a href=" admin_url().'/admin.php?page=ndotm-admin-menu-slug/">Here</a>';
		
		//Register menu for this specific plugin's menu page. Then use the function below to add page content.
		add_submenu_page( 'ndotm-admin-menu-slug', 'Hardwood', 'Hardwood Options', 'manage_options', 'ndotm-hardwood-admin-menu-slug','ndotm_hardwood_admin_page_func');
	}
	add_action('admin_menu', 'ndotm_hardwood_admin_menu_func');

		/**
		* Output page content for hardwood backend page when plugin is active.
		*
		* You can optionally also hook into the main N.M Generic page with {@action ndotm_admin_page_before_action} or {@filter ndotm_admin_page_content_filter} or {@action ndotm_admin_page_after_action}.
		*
		* @action ndotm_hardwood_admin_page_before_action
		* @filter ndotm_hardwood_admin_page_content_filter
		* @action ndotm_hardwood_admin_page_after_action
		* @todo Basically everything. If you are using the Settings API to save data, and need the user to be other than the administrator, will need to modify the permissions via the hook option_page_capability_{$option_group}, where $option_group is the same as option_group in register_setting() . Check out the Settings API.
		*/
			function ndotm_hardwood_admin_page_func(){
				//echo page content here
				do_action('ndotm_hardwood_admin_page_before_action');
				require('html-example/delivery-event-setup.php');
				//NEED to add handling for any form in the above. For now...hardcode:
				do_action('ndotm_hardwood_admin_page_after_action');
			}
		//END Output page content for hardwood backend page when plugin is active. TEST:  echo '<a href=" admin_url().'/admin.php?page=ndotm-hardwood-admin-menu-slug/">Here</a>';
?>