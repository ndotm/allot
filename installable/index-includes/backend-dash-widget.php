<?php
	/**
	* Create a widget on the WP backend dashboard f0r the Hardwood plugin
	*
	* @todo Move styles to an admin_css? And sstill filter it?
	* @action ndotm_hardwood_admin_dash_widget_before_action
	* @filter ndotm_hardwood_admin_dash_widget_styles_filter
	* @filter ndotm_hardwood_admin_dash_widget_html_filter
	* @action ndotm_hardwood_admin_dash_widget_after_action
	*/
	function ndotm_hardwood_admin_dash_func() {
		add_meta_box('ndotm_hardwood_admin_dash_widget_id', 'Nineteen.Marketing Adjective Hardwood - An Actual Plugin, and so can you!', 'ndotm_hardwood_admin_dash_widget_func', 'dashboard', 'side');
	}
	

	function ndotm_hardwood_admin_dash_widget_func() {
		do_action('ndotm_hardwood_admin_dash_widget_before_action');
		echo apply_filters('ndotm_hardwood_admin_dash_widget_styles_filter','
			<style>
				#nineteen-logo {
					width: 30%;
					display: block;
					margin: 20px auto;
				}
				#nineteen-intro {
					text-align:center;	
					color:'.$ndotm_hardwood_admin_page_inputed_theme_color.';
				}
				#custom-color-list li span {
					width:20px;
					height: 20px;
					border:1px solid #000000;	
				}
			</style>		
		');
		echo apply_filters('ndotm_hardwood_admin_dash_widget_html_filter','
		<div>
			<p id="nineteen-intro">This theme has been customized by <a href="'.NINETEEN_URL.'" target="_blank">Nineteen.Marketing</a>.</p>
			<a target="_blank" href="'.NINETEEN_URL.'"><img id="nineteen-logo" src="'.NDOTM_HARDWOOD_PLUGIN_URL.'/img/n.m.png" alt="Nineteen.Marketing" /></a>
			<p>Any customizations done by Nineteen.Marketing are located in the Nineteen.Marketing Customizations Plugin. This includes the following:</p>
			<ul style="list-style-type:disc;padding-left:20px;">
				<li><a href="'.get_bloginfo('url').'/wp-admin/plugin-editor.php?file=pro119_customizations%2Fpro119_custom.css">Custom CSS</a></li>
				<li><a href="'.get_bloginfo('url').'/wp-admin/plugin-editor.php?file=pro119_customizations%2Fpro119_custom.js">Custom jQuery</a></li>
			</ul>
			<p>If you have any questions about anything, please <a target="_blank" href="'.NINETEEN_URL.'/contact-us/">contact us</a> or call <a href="tel:208-779-0119">(208) 779-0119</a>.  Thanks.</p>
		</div>');
		do_action('ndotm_hardwood_admin_dash_widget_after_action');
	}

	add_action( 'wp_dashboard_setup', 'ndotm_hardwood_admin_dash_func' );
//End Create a widget on the WP backend dashboard. View dashboard when logged in and when plugin is activated to test.

?>