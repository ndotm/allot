<HTML>

<?php
	include('header.html');
?>

<BR><BR>

<?php

	parse_str($_SERVER["QUERY_STRING"]);
	
  require('ini.php');	
  require('DBConnect.php');
	
	$conn = DBConnect();
	
	if ($cmp == '*new') {
		
		$dbaction = 'A';
		$marketingcampaignId = '';
		$marketingcampaigndesc = '';
		$fromdate = '';
		$todate = '';
		
	} else {

		$dbaction = 'U';
		
		$sql = 'SELECT marketingcampaigndesc, fromdate, todate'
			. '	FROM vs_marketingcampaign'
			. ' WHERE marketingcampaignId = '
			. $cmp;
 
		$result = $conn->query($sql);
	
		$row = $result->fetch_row();
		$marketingcampaignId = $cmp;
		$marketingcampaigndesc = $row[0];
		$fromdate = $row[1];
		$todate = $row[2];
		
	}

	echo  
		'<FORM METHOD=POST ACTION="U_MarketingCampaign.php">'
		. '<INPUT TYPE=HIDDEN NAME=dbaction VALUE="'
		. $dbaction
		. '">'
		. '<INPUT TYPE=HIDDEN NAME=marketingcampaignId VALUE="'
		. $marketingcampaignId
		. '">'
		. '<TABLE>'
		. '<TR>'
		. '<TD>Marketing Campaign</TD>'
		. '<TD>'
		. $marketingcampaignId
		. '</TD>'
		. "</TR>"
		. '<TR>'
		. '<TD>Description</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=64 NAME=marketingcampaigndesc VALUE="'
		. $marketingcampaigndesc
		. '"><INPUT TYPE=HIDDEN NAME=w_marketingcampaigndesc VALUE="'
		. $marketingcampaigndesc
		. '"></TD>'
		. "</TR>"
		. '<TR>'
		. '<TD>Begin Date</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=6 NAME=fromdate VALUE="'
		. $fromdate
		. '" PATTERN="'
		. $datePattern	
		. '"><INPUT TYPE=HIDDEN NAME=w_fromdate VALUE="'
		. $fromdate
		. '"</TD>'
		. '</TR>'
			. '<TD>End Date</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=6 NAME=todate VALUE="'
		. $todate
		. '" PATTERN="'
		. $datePattern
		. '"><INPUT TYPE=HIDDEN NAME=w_todate VALUE="'
		. $todate
		. '"</TD>'
		. '</TR>'
		. '<TR><TD></TD><TD></TD></TR>'

	;

	if ($cmp != '*new') {
		
		echo '<TR><TH ALIGN=LEFT COLSPAN=2><I>Channels Used to Deliver Campaign</I></TH></TR>';		
		echo '<TR><TH ALIGN=CENTER>URL Parameter<BR>(?allot=)</TH><TH ALIGN=LEFT>Marketing<BR>Channel</TH></TR>';
				
		$sql = 'SELECT marketingdeliveryid, marketingchannelcd, marketingdeliverydesc'
			. '	FROM vs_marketingdelivery'
			. ' WHERE marketingcampaignId = '
			. $marketingcampaignId
		;
		
		$result = $conn->query($sql);

		if ($result->num_rows == 0) {
			echo "<TR><TD COLSPAN=2>No associated marketing channel delivery</TD></TR>";
			
		} else {
		
			for ($i = 0; $i <= $result->num_rows; ++$i)	{
				$row = $result->fetch_row();
	
				echo 
					'<TR>'
					. '<TD ALIGN=CENTER>'
					. '<A HREF=MarketingDelivery.php?cmp='
					. $marketingcampaignId
					. '&allot='
					. $row[0]
					. '>'
					. $row[0]
					. '</A>'
					. '</TD>'
					. '<TD>'
					. $row[1] . ' ' . $row[2]
					. '</TD>'
					. '</TR>'
				;
			}
		}
	}
	
	echo 
		'<TR><TD COLSPAN=2><A HREF=MarketingDelivery.php?cmp='
		.	$marketingcampaignId
		. '&allot=*new>Add Channel Delivery</A></TD></TR>';

	$conn->close();
	
	echo "</TABLE>";
	echo "<INPUT TYPE=SUBMIT VALUE=Update>";
	echo "</FORM>";

?>


<BR>
<A HREF=MarketingCampaigns.php>Return to Marketing Campaign List</A>


</HTML>