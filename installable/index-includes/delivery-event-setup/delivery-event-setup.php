<?php 
$Marketing_Delivery_lines1_69;//Convert the functionality from MarketingDelivery.php lines 1-68 into WP stuff and put the result (allot=X) here
$Marketing_Delivery_lines1_101;//Convert the functionality from MarketingDelivery.php lines 1-101 into WP stuff and put the result (marketing channel select value) here
	//list of select options is in javascript
	
	
echo apply_filters('ndotm_hardwood_admin_page_content_filter','
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plugin Setup</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Navigation-Clean.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Steps-Progressbar.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider.css">
</head>

<body>
    <main class="page landing-page delivery-event-setup">
        <form>
            <section class="clean-block clean-info dark" style="background-image:url(&quot;assets/img/DeathtoStock_Wired6.jpg&quot;);background-size:cover;padding:0px;">
                <div style="background-color:rgba(59,153,224,0.90);width:100%;height:100%;padding:0 0 100px;">
                    <div class="container">
                        <div class="text"></div>
                        <div class="block-heading">
                            <h2 class="text-light">Time To Do Marketing!<br></h2>
                            <p>Each time you create marketing activity, you should<br></p>
                            <p class="little-wider">1. set up a Delivery Event (i.e. "mother\'s day first email"),<br></p>
                            <p class="little-wider">2.&nbsp;organize Events into Campaigns ("mother\'s day campaign") and Channels ("email", "SEO", etc.) to&nbsp;see reports on results of each!<br></p>
                            <p class="little-wider">3.&nbsp;use the Tag on every link in that Event to see related conversions<br></p>
                        </div>
                        <div class="form-row align-items-center" style="background-size:cover;">
                            <div class="col-md-6 align-self-stretch" style="background-size:cover;">
                                <h2 class="text-light">New Marketing Event</h2>
                                <div class="col-md-12 feature-box" style="margin:-1 0 15px;"><i class="icon-share icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                    <h4>Channel/Medium<br></h4><input class="form-control" type="text" id="channels" value="'.$Marketing_Delivery_lines1_101.'">
                                    <p>What channel will you use to deliver this?</p>
                                </div>
                            </div>
                            <div class="col-md-6 align-self-stretch" style="background-size:cover;">
                                <h2 class="text-light">Use This Stuff</h2>
                                <div class="col-md-12 feature-box" style="margin:0px;"><i class="icon-link icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                    <h4>Tag<br></h4><input class="form-control" type="text" value="?allot='.$Marketing_Delivery_lines1_69.'2" readonly="">
                                    <p>Add that to every link in this Event. You can link to any page on your site!</p>
                                </div>
                            </div>
                            <div class="col">
                                <div><a class="btn btn-outline-light btn-lg" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-1" role="button" href="#collapse-1" style="background-color:rgba(0,123,255,0);box-shadow:none;border:none;"><div><button class="btn btn-success btn-lg d-inline" type="submit" style="margin:20px;">Save!</button><a class="btn btn-outline-light btn-lg" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-1" role="button" href="#collapse-1" style="/*background-color:rgba(0,123,255,0);*//*box-shadow:none;*//*border:none;*/"><strong><i class="fa fa-plus"></i>&nbsp;Add to campaign or add other details</strong></a></a>
                                    <div
                                        class="collapse" id="collapse-1">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="col-md-12 feature-box" style="margin:0px;"><i class="icon-umbrella icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                                    <h4>Campaign<br></h4><input class="form-control" type="text" id="campaigns-list">
                                                    <p>Is this Delivery Event part of a bigger effort? Use this to group Events together in reporting.</p>
                                                </div>
                                                <div class="col-md-12 feature-box" style="margin:0px;"><i class="icon-speech icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                                    <h4>Description<br></h4><input class="form-control" type="text">
                                                    <p>Keep yourself organized!</p>
                                                </div><button class="btn btn-outline-light btn-lg" type="button" id="add-meta" style="margin:0 0 20px 20px;">Add Meta</button></div>
                                            <div class="col">
                                                <div class="col-md-12 feature-box" style="margin:0px;"><i class="icon-graph icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                                    <h4>UTM<br></h4><input class="form-control" type="text" value="?allot=2&amp;utm_source=newsletter&amp;utm_medium=email&amp;utm_campaign=monther\'s%20day" readonly="">
                                                    <p>Add that to every link in this Event. You can link to any page on your site!</p>
                                                </div>
                                                <div class="col-md-12 feature-box" style="margin:-1 0 15px;"><i class="icon-doc icon" data-bs-hover-animate="pulse" style="color:rgb(255,255,255);"></i>
                                                    <h4>Link a WordPress Page?<br></h4><input class="form-control" type="text" id="list-of-wp-pages">
                                                    <p>What channel will you use to deliver this?</p>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        </form>
    </main>
    <div>
        <nav class="navbar navbar-light navbar-expand-md navigation-clean">
            <div class="container"><a class="navbar-brand" href="#">Allot</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="reports-dashboard.html">Reports Dashboard</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="plugin-setup-page-example.html">Plugin Setup</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="delivery-event-setup.html">Delivery Event</a></li>
                        <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Dropdown </a>
                            <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">First Item</a><a class="dropdown-item" role="presentation" href="#">Second Item</a><a class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/bs-animation.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
    <script src="assets/js/Swiper-Slider.js"></script>
    <script src="assets/js/untitled.js"></script>
</body>

</html>'); ?>