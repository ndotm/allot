<?php
/**
* WP Action Function - Enqueue all javascripts for this plugin, including jquery and our main_custom_js.php for this plugin.
* 
* You will need to uncomment each line that you want. $ndotm_hardwood_scripts_required_arr will hold all the required dependancies for our main_custom. As always, find and replace "hardwood" and "HARDWOOD" with your wood. Please make sure you don't include scripts that are already in WP core (see below). These are PHP style/script sheets and they can use varialbes, PASSED AS GETS (not WP) like what page you are on or something like "theme colors" which you may set on the admin page. Use the page to avoid conflicts, however, don't worry too much about trying to make scripts "smaller" by sending functions on a page-by-page basis, because our performace is much more likely to suffer because of PHP performace than filesize.https://css-tricks.com/css-variables-with-php/. Definitely update your version, even as you dev, to help with cahce issues.
* 
* @todo add page or function-specific conditionality - only enqueue what you actually need in each context. Reduce overhead.
* @see https://developer.wordpress.org/reference/functions/wp_enqueue_script/ Which has links to documentation for ALL of these.
*/
	function ndotm_hardwood_init_enqueue_func() {
		//JAVASCRIPTZ
				
				// Will hold all the required dependancies for our main_custom
				$ndotm_hardwood_scripts_required_arr=array();
				
				//All of the following are included in WP core - NO REASON TO INCLUDE!
				//wp_enqueue_script('jquery');$ndotm_hardwood_scripts_required_arr[]='jquery';
					//if you use something indented to this level, you need jquery right there^
					//wp_enqueue_script('jquery-ui-core');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-core';
						//if you use something indented to this level, you need jquery-ui-core right there^
						//wp_enqueue_script('jquery-effects-core');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-core';
							//if you use something indented to this level, you need jquery-effects-core right there^
							//wp_enqueue_script('jquery-effects-blind');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-blind';
							//wp_enqueue_script('jquery-effects-bounce');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-bounce';
							//wp_enqueue_script('jquery-effects-clip');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-clip';
							//wp_enqueue_script('jquery-effects-drop');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-drop';
							//wp_enqueue_script('jquery-effects-explode');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-explode';
							//wp_enqueue_script('jquery-effects-fade');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-fade';
							//wp_enqueue_script('jquery-effects-fold');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-fold';
							//wp_enqueue_script('jquery-effects-highlight');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-highlight';
							//wp_enqueue_script('jquery-effects-pulsate');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-pulsate';
							//wp_enqueue_script('jquery-effects-scale');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-scale';
							//wp_enqueue_script('jquery-effects-shake');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-shake';
							//wp_enqueue_script('jquery-effects-slide');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-slide';
							//wp_enqueue_script('jquery-effects-transfer');$ndotm_hardwood_scripts_required_arr[]='jquery-effects-transfer';
					//wp_enqueue_script('jquery-form');$ndotm_hardwood_scripts_required_arr[]='jquery-form';
					//wp_enqueue_script('jquery-color');$ndotm_hardwood_scripts_required_arr[]='jquery-color';
					//wp_enqueue_script('jquery-masonry');$ndotm_hardwood_scripts_required_arr[]='jquery-masonry';
					//wp_enqueue_script('jquery-ui-core');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-core';
					//wp_enqueue_script('jquery-ui-widget');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-widget';
					//wp_enqueue_script('jquery-ui-accordion');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-accordion';
					//wp_enqueue_script('jquery-ui-autocomplete');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-autocomplete';
					//wp_enqueue_script('jquery-ui-button');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-button';
					//wp_enqueue_script('jquery-ui-datepicker');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-datepicker';
					//wp_enqueue_script('jquery-ui-dialog');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-dialog';
					//wp_enqueue_script('jquery-ui-draggable');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-draggable';
					//wp_enqueue_script('jquery-ui-droppable');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-droppable';
					//wp_enqueue_script('jquery-ui-menu');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-menu';
					//wp_enqueue_script('jquery-ui-mouse');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-mouse';
					//wp_enqueue_script('jquery-ui-position');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-position';
					//wp_enqueue_script('jquery-ui-progressbar');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-progressbar';
					//wp_enqueue_script('jquery-ui-selectable');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-selectable';
					//wp_enqueue_script('jquery-ui-resizable');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-resizable';
					//wp_enqueue_script('jquery-ui-selectmenu');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-selectmenu';
					//wp_enqueue_script('jquery-ui-sortable');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-sortable';
					//wp_enqueue_script('jquery-ui-slider');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-slider';
					//wp_enqueue_script('jquery-ui-spinner');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-spinner';
					//wp_enqueue_script('jquery-ui-tooltip');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-tooltip';
					//wp_enqueue_script('jquery-ui-tabs');$ndotm_hardwood_scripts_required_arr[]='jquery-ui-tabs';
					//wp_enqueue_script('wp-mediaelement');$ndotm_hardwood_scripts_required_arr[]='wp-mediaelement';
					//wp_enqueue_script('schedule');$ndotm_hardwood_scripts_required_arr[]='schedule';
					//wp_enqueue_script('suggest');$ndotm_hardwood_scripts_required_arr[]='suggest';
					//wp_enqueue_script('hoverIntent');$ndotm_hardwood_scripts_required_arr[]='hoverIntent';
					//wp_enqueue_script('jquery-hotkeys');$ndotm_hardwood_scripts_required_arr[]='jquery-hotkeys';
					//wp_enqueue_script('iris');$ndotm_hardwood_scripts_required_arr[]='iris'; //Iris (Colour picker)
				//wp_enqueue_script('tiny_mce');$ndotm_hardwood_scripts_required_arr[]='tiny_mce';
				//wp_enqueue_script('thickbox');$ndotm_hardwood_scripts_required_arr[]='thickbox';
				//wp_enqueue_script('jcrop');$ndotm_hardwood_scripts_required_arr[]='jcrop';
				//wp_enqueue_script('swfobject');$ndotm_hardwood_scripts_required_arr[]='swfobject';
				//wp_enqueue_script('swfupload');$ndotm_hardwood_scripts_required_arr[]='swfupload';
				//wp_enqueue_script('sack');$ndotm_hardwood_scripts_required_arr[]='sack'; //Simple AJAX Code-Kit
				//wp_enqueue_script('quicktags');$ndotm_hardwood_scripts_required_arr[]='quicktags';
				//wp_enqueue_script('backbone');$ndotm_hardwood_scripts_required_arr[]='backbone';
					//wp_enqueue_script('underscore');$ndotm_hardwood_scripts_required_arr[]='underscore';
				
				//Or just manually override dependencies here
				//$ndotm_hardwood_scripts_required_arr=array('jquery','jquery-ui-core');
        
        wp_enqueue_script( 'ndotm-hardwood-main-custom-script', NDOTM_HARDWOOD_PLUGIN_URL.'/ndotm_hardwood_main_custom_js.php', $ndotm_hardwood_scripts_required_arr, '1.0', true );
		
		/**
		* CSS STYLES. 
		* Again remember these are PHP style/script sheets and they can use varialbes PASSED AS GETS (not WP), like what page you are on or something like "theme colors" which you may set on the admin page. Use the page to avoid conflicts, however, don't worry too much about trying to make scripts "smaller" by sending functions on a page-by-page basis, because our performace is much more likely to suffer because of PHP performace than filesize.https://css-tricks.com/css-variables-with-php/. Definitely update your version, even as you dev, to help with cahce issues. 
		*/
		
		//Dependent styles if you need them  -  use first string on any styles/scripts needed to run this one. IE if we had a framework, or core of favorites.
		$ndotm_hardwood_init_enqueue_dependant_styles_arr = array(		
		);
		
		//Accepts media types like 'all', 'print' and 'screen', or media queries like '(orientation: portrait)' and '(max-width: 640px)'.
		wp_enqueue_style( 'ndotm-hardwood-main-custom-style', NDOTM_HARDWOOD_PLUGIN_URL.'/ndotm_hardwood_main_custom_css.php', $ndotm_hardwood_init_enqueue_dependant_styles_arr, '1.0.0', 'all');
    }
   	add_action('wp_enqueue_scripts','ndotm_hardwood_init_enqueue_func');
// END WP Action Function - Enqueue all javascripts for this plugin, including jquery and our main_custom_js.php for this plugin. To test, inspect the front end and look for each of the uncommented scripts, and main_custom_js.php in <link tags
?>