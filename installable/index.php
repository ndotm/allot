<?php 
/*
Plugin Name: Nineteen.Marketing Adjective Hardwood - An Actual Plugin (tempalte)!
Plugin URI: http://Nineteen.Marketing/hardwood
Description: This plugin creates is a template for creating plugins. It has all the files and structure for an NdotM series plugin. 1. Be sure to delete, code out, un-include etc. anything you don't need - keel overhead low. 2. To start, search for any "hardwood", "HARDWOOD", or "Hardwood" and replace with your wood, INCLUDING filenames.
Author: Nineteen.Marketing
Version: 1.0
Author URI: http://Nineteen.Marketing/about-nineteen-marketing/
*/

/**
* Defining plugin URLs. Sets up some variables about the plugin like dir.
*/
if (!defined('NDOTM_HARDWOOD_THEME_DIR'))
    define('NDOTM_HARDWOOD_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
if (!defined('NDOTM_HARDWOOD_PLUGIN_NAME'))
    define('NDOTM_HARDWOOD_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));
if (!defined('NDOTM_HARDWOOD_PLUGIN_DIR'))
    define('NDOTM_HARDWOOD_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . NDOTM_HARDWOOD_PLUGIN_NAME);
if (!defined('NDOTM_HARDWOOD_PLUGIN_URL'))
    define('NDOTM_HARDWOOD_PLUGIN_URL', WP_PLUGIN_URL . '/' . NDOTM_HARDWOOD_PLUGIN_NAME);
if (!defined('NINETEEN_URL'))
    define('NINETEEN_URL', 'https://nineteen.marketing');
// END Include the config file. Test: echo 'NDOTM_HARDWOOD_THEME_DIR: '.NDOTM_HARDWOOD_THEME_DIR.'NDOTM_HARDWOOD_PLUGIN_NAME: '.NDOTM_HARDWOOD_PLUGIN_NAME.'NDOTM_HARDWOOD_PLUGIN_DIR: '.NDOTM_HARDWOOD_PLUGIN_DIR.'NDOTM_HARDWOOD_PLUGIN_URL: '.NDOTM_HARDWOOD_PLUGIN_URL.'NINETEEN_URL: '.NINETEEN_URL;

/**
* Include the js scripts we need.
*/
	include 'index-includes/enqueue-all-scripts.php';
//To test, inspect a front end page when the plugin is active, and look for these files linked in the header/footer.
	
/**
* Add a page to the wordpress backend menu for this plugin, under the N.M menu.
*/
	include 'index-includes/backend-menu-page.php';

/**
* Add a Widget to the backend dashboard (NOT the Oak dash, by the way)
*/
	include 'index-includes/backend-dash-widget.php';

/**
* Assign this plugin's page templates when a visiter pulls a relevant page.
* 
* This area needs a lot of work - how to integrate our function into your theme? Add template to theme? Assign a page? Associate page to template?
* @todo Clean up and integrate
* @todo Does setup have to always run? or like just on the backend or something?
*/
	include 'post-type-includes/assign-templates.php';
	include 'post-type-includes/setup-bookshelf-post-type.php';
	
/**
* Pretty much all your other programming, hooks and actions, etc.
*/
	include 'functions.php';	
?>