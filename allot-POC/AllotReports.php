<?php
	include('header.html');
?>

<body>

<canvas id="myCanvas" width=320 height=240>
</canvas>

<script>

	function drawLine(ctx, startX, startY, endX, endY) {
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(endX,endY);
    ctx.stroke();
	}
	
	function drawArc(ctx, centerX, centerY, radius, startAngle, endAngle) {
    ctx.beginPath();
		ctx.lineWidth = 2;
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
		ctx.strokeStyle = '#003300';
    ctx.stroke();
	}
	function drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color ) {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX,centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
		ctx.stroke();
	}

  var canvas = document.getElementById('myCanvas');
  var context = canvas.getContext('2d');
  var centerX = canvas.width / 2;
  var centerY = canvas.height / 2;
  var radius = 80;
	
/*
	drawArc(context, centerX, centerY, radius, 0, 2 * Math.PI * .888)
	drawPieSlice(context, centerX, centerY, radius, 0, 2 * Math.PI * .888, '#ff0000');
	drawArc(context, centerX, centerY, radius, 2 * Math.PI * .888, 2 * Math.PI)
	drawPieSlice(context, centerX, centerY, radius, 2 * Math.PI * .888, 2 * Math.PI, '#00ff00');
	*/

</script>


<?php

	require('DBConnect.php');
	
	$conn = DBConnect();

	$sql = "SELECT a.marketingdeliveryId, b.marketingdeliverydesc, b.marketingchannelcd, b.marketingcampaignId"
	  . ", a.attributedconversions, a.attributedvalue"
		. ", c.totalAttrib, d.ChartColor"
		. " FROM vs_attribution a"
		.	" JOIN vs_marketingdelivery b on b.marketingdeliveryId = a.marketingdeliveryId"
		. " CROSS JOIN ("
		. " SELECT SUM(attributedvalue) AS totalAttrib"
		. " FROM vs_attribution"
		. " ) c"
		. " JOIN vs_marketingchannel d ON d.marketingchannelcd = b.marketingchannelcd";
	
	$result = $conn->query($sql);

	if ($result->num_rows == 0) {
		echo "No attribution to report";
			
	} else {
		
		echo 
			"<TABLE>"
			. "<TR>"
			. "<TH>allot=</TH><TH>Description</TH><TH>Marketing Channel</TH><TH>Campaign</TH><TH>Attributed Conversions</TH><TH>Attributed Value</TH>";
		
		$accum = 0;
		
		for ($i = 0; $i < $result->num_rows; ++$i)	{
			$row = $result->fetch_row();
			
			$thisAttrib = floatval($row[5]);
			$totAttrib = floatval($row[6]);
			$pctAttrib = $thisAttrib / $totAttrib;
			$color = $row[7];
	
			echo 
				'<TR>'
				. '<TD ALIGN=CENTER>'
				. $row[0]
				. '</TD><TD>'
				. $row[1]
				. '</TD><TD>'
				. $row[2]
				. '</TD><TD>'
				. $row[3]
				. '</TD><TD>'
				. number_format($row[4], 2)
				. '</TD><TD>'
				. '$' . number_format($thisAttrib, 2)
				. '</TD>'
				. '</TR>'
			;
			echo 
				'<script>'
				. 'drawPieSlice(context, centerX, centerY, radius, 2 * Math.PI * ' . $accum . ', 2 * Math.PI * ' . ($pctAttrib + $accum) . ', "' . $color . '")'
				. '</script>';
				
			$accum += $pctAttrib;
			
		}
	}

	$conn->close();
	
?>

</body>
 </html>