<?php

	$dbaction = $_POST['dbaction'];
	$marketingchannelcd = $_POST['marketingchannelcd'];
	$marketingchanneldesc = $_POST['marketingchanneldesc'];
	$w_marketingchanneldesc = $_POST['w_marketingchanneldesc'];
	$ChartColor = $_POST['ChartColor'];
	$w_ChartColor = $_POST['w_ChartColor'];
	$lagdays = $_POST['lagdays'];
	$w_lagdays = $_POST['w_lagdays'];
	
	require('DBConnect.php');
	$conn = DBConnect();
	
	
	if ($dbaction == 'A') {
		
		$sql = "INSERT INTO vs_marketingchannel"
			. " (marketingchannelcd, marketingchanneldesc, lagdays, ChartColor)"
			. " VALUES ('"
			. $marketingchannelcd
			. "', '"
			. $marketingchanneldesc
			. "', "
			. $lagdays
			. "'"
			. $ChartColor
			. "')"
		;

		$result = $conn->query($sql);	
		
	} else {
		
		if ($marketingchanneldesc != $w_marketingchanneldesc || $lagdays != $w_lagdays || $ChartColor != $w_ChartColor) {
	
			$sql = "UPDATE vs_marketingchannel"
				. " SET marketingchanneldesc = '" . $marketingchanneldesc . "'"
				. " , lagdays = " . $lagdays
				. ", ChartColor = '" . $ChartColor . "'"
				. " WHERE marketingchannelcd = '" . $marketingchannelcd . "'"
			;
			$result = $conn->query($sql);
	
		}
			
		if ($lagdays != $w_lagdays) {
		
			$sql = "DELETE FROM vs_marketingchannellag"
				. " WHERE marketingchannelcd = '"
				. $marketingchannelcd
				. "'"
				. " AND dayno > "
				. $lagdays
			;
			$conn->query($sql);
		}
	
		for ($i = 0; $i <= $lagdays; ++$i)	{
		
			$id = 'marketingchannellagid' . $i;
			$w_var = 'w_influence' . $i;
			$var = 'influence' . $i;
				
			if (
				(isset($_POST[$w_var]) && isset($_POST[$var]) && ($_POST[$w_var] != $_POST[$var]))
				|| (isset($_POST[$w_var]) == FALSE)
				|| (isset($_POST[$var]) == FALSE) 
				) 
			{ 
				if (isset($_POST[$var]) == FALSE) {
					$var = 0;
				}
				if (isset($_POST[$id]) && $_POST[$id] != "") {
					$sql = "UPDATE vs_marketingchannellag"
						. " SET influence = " . $_POST[$var]
						. " WHERE marketingchannellagid = " . $_POST[$id]
					;
				} else {
					$sql = "INSERT INTO vs_marketingchannellag"
						. " (marketingchannelcd, dayno, influence)"
						. " VALUES('"
						. $marketingchannelcd
						. "', "
						. $i
						. ", "
						. $_POST[$var]
						. ")"
					;
				}
				
				$conn->query($sql);	
			
			}
		}
	}
		
	$conn->close();

	$toURL = 'Location: //localhost/allot/MarketingChannel.php?mc='
		. $marketingchannelcd;

	header($toURL, true, 301);
	exit();

?>