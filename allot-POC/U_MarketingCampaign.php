<?php

	$dbaction = $_POST['dbaction'];
	$marketingcampaignId = $_POST['marketingcampaignId'];
	$marketingcampaigndesc = str_replace("'", "\'", $_POST['marketingcampaigndesc']);
	$w_marketingcampaigndesc = str_replace("'", "\'", $_POST['w_marketingcampaigndesc']);
	$fromdate = $_POST['fromdate'];
	$w_fromdate = $_POST['w_fromdate'];
	$todate = $_POST['todate'];
	$w_todate = $_POST['w_todate'];
		
	require('DBConnect.php');
	$conn = DBConnect();
	
	if ($dbaction == 'A') {
		
		$sql = "INSERT INTO vs_marketingcampaign"
			. " (marketingcampaigndesc, fromdate, todate)"
			. " VALUES ('"
			. $marketingcampaigndesc
			. "', "
			. "STR_TO_DATE('". $fromdate . "', '%Y-%m-%d')"
			. ", "
			. "STR_TO_DATE('". $todate . "', '%Y-%m-%d')"
			. ")";
		;
		
		$result = $conn->query($sql);			
		$marketingcampaignId = $conn->insert_id;
		
	} else {
		
		if ($marketingcampaigndesc != $w_marketingcampaigndesc || $fromdate != $w_fromdate || $todate != $w_todate) {
	
			$sql = "UPDATE vs_marketingcampaign"
				. " SET marketingcampaigndesc = '" . $marketingcampaigndesc . "'"
				. " , fromdate = " . "STR_TO_DATE('". $fromdate . "', '%Y-%m-%d'))"
				. " , todate = " . "STR_TO_DATE('". $todate . "', '%Y-%m-%d'))"
				. " WHERE marketingcampaignId = " . $marketingcampaignId
			;

			$result = $conn->query($sql);
	
		}

	}
		
	$conn->close();

	$toURL = 'Location: //localhost/allot/MarketingCampaign.php?cmp='
		. $marketingcampaignId;

	header($toURL, true, 301);
	exit();

?>