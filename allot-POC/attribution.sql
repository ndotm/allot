CREATE TEMPORARY TABLE tmp_EVENTINFLUENCE01 (
 conversionId VARCHAR(32),
 marketingchannelcd VARCHAR(8),
 marketingdeliveryId INTEGER,
 dayno INTEGER
);
INSERT INTO tmp_EVENTINFLUENCE01
 (conversionId, marketingchannelcd, marketingdeliveryId, dayno)
 SELECT a.conversionId, c.marketingchannelcd, b.marketingdeliveryId, 
  FLOOR(TIMESTAMPDIFF(SECOND, b.eventtime, a.eventtime) / 86400) 
 FROM `vs_conversionevent` a 
 JOIN vs_marketingcampaignevent b on b.vs_visitorId = a.vs_visitorId
 JOIN vs_marketingdelivery c ON c.marketingdeliveryId = b.marketingdeliveryId
 JOIN (
  SELECT marketingchannelcd, MAX(Lagdays) AS maxLagDays
  FROM vs_marketingchannel
  GROUP BY marketingchannelcd
 ) d ON d.marketingchannelcd = c.marketingchannelcd
 WHERE a.eventtime >= b.eventtime - INTERVAL d.maxLagDays DAY;
 DELETE FROM vs_influence
 WHERE conversionId IN (
  SELECT DISTINCT conversionId
  FROM tmp_EVENTINFLUENCE01
 );
 INSERT INTO vs_influence
 (conversionId, marketingdeliveryId, influence)
 SELECT a.conversionId, a.marketingdeliveryId, b.influence
 FROM tmp_EVENTINFLUENCE01 a
 JOIN vs_marketingchannellag b ON b.marketingchannelcd = a.marketingchannelcd AND b.dayno = a.dayno;
 
 CREATE TEMPORARY TABLE tmp_EVENTINFLUENCE02 (
  conversionId VARCHAR(32),
  marketingdeliveryId INTEGER,
  influence INTEGER,
  AdjInfluence FLOAT
 );
 INSERT INTO tmp_EVENTINFLUENCE02
 (conversionId, marketingdeliveryId, influence, AdjInfluence)
 SELECT a.conversionId, a.marketingdeliveryId, a.influence, 
  (a.influence * (CASE WHEN sumInfluence > 100 THEN 100 / sumInfluence ELSE 1 END)) / 100 AS AdjInfluence
 FROM vs_influence a
 JOIN (
  SELECT conversionId, SUM(influence) AS sumInfluence
  FROM vs_influence
  GROUP BY conversionId
 ) b ON b.conversionId = a.conversionId;
 DELETE FROM vs_attribution;
 INSERT INTO vs_attribution
 (marketingdeliveryId, attributedconversions, attributedvalue)
 SELECT a.marketingdeliveryId, SUM(a.AdjInfluence), SUM(a.AdjInfluence * conversionvalue)
 FROM tmp_EVENTINFLUENCE02 a
 JOIN vs_conversionevent b ON b.conversionId = a.conversionId
 GROUP BY a.marketingdeliveryId;