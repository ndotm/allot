<?php

	function getVisitorId() {
	
		if (isset($_COOKIE['vs_visitor'])) $vs_visitorId = $_COOKIE['vs_visitor'];
		else {
			$v1 = time();
			$v2 = $_SERVER['REMOTE_ADDR'];
			$v3 = rand(1, 100);
	
			$vs_visitorId = hash('ripemd128', $v1 . $v2 . $v3);
	
			$cookie_exp = mktime(23, 59, 59, 12, 31, 2037);
		
			setcookie('vs_visitor', $vs_visitorId, $cookie_exp);
		}
	
		return $vs_visitorId;
		
	}	

?>