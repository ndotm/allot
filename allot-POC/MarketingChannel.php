<HTML>

<?php
	include('header.html');
?>


<?php

	parse_str($_SERVER["QUERY_STRING"]);
	
  require('DBConnect.php');
	
	$conn = DBConnect();
	
	if ($mc == '*new') {
		
		$dbaction = 'A';
		$marketingchannelcd = '';
		$marketingchanneldesc = '';
		$lagdays = '';
		$ChartColor = '';
		
	} else {

		$dbaction = 'U';
		
		$sql = 'SELECT marketingchanneldesc, lagdays, ChartColor'
			. '	FROM vs_marketingchannel'
			. ' WHERE marketingchannelcd = "'
			. $mc
			. '"';
 
		$result = $conn->query($sql);
	
		$row = $result->fetch_row();
		$marketingchannelcd = $mc;
		$marketingchanneldesc = $row[0];
		$lagdays = $row[1];
		$ChartColor = $row[2];
		
	}

	echo  
		'<FORM METHOD=POST ACTION="U_MarketingChannel.php">'
		. '<INPUT TYPE=HIDDEN NAME=dbaction VALUE="'
		. $dbaction
		. '">'
		. '<TABLE>'
		. '<TR>'
		. '<TD>Marketing Channel</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=8 NAME=marketingchannelcd';
		
	if ($dbaction == 'U') {
		echo ' READONLY="readonly"';
	}
	
	echo
		' VALUE="'
		. $marketingchannelcd
		. '"></TD>'
		. "</TR>"
		. '<TR>'
		. '<TD>Description</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=64 NAME=marketingchanneldesc VALUE="'
		. $marketingchanneldesc
		. '"><INPUT TYPE=HIDDEN NAME=w_marketingchanneldesc VALUE="'
		. $marketingchanneldesc
		. '"></TD>'
		. "</TR>"
		. '<TR>'
		. '<TD>Chart Color</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=7 NAME=ChartColor VALUE="'
		. $ChartColor
		. '"><INPUT TYPE=HIDDEN NAME=w_ChartColor VALUE="'
		. $ChartColor
		. '">&nbsp;&nbsp;&nbsp;&nbsp;'
		. '<span style="background-color: '
		. $ChartColor
		. '">&nbsp;&nbsp;&nbsp;&nbsp;</span></TD>'
		. "</TR>"
		. '<TR>'
		. '<TD>Lag Days</TD>'
		. '<TD><INPUT TYPE=TEXT SIZE=3 NAME=lagdays VALUE="'
		. $lagdays
		. '" PATTERN="\d*">'
		. '<INPUT TYPE=HIDDEN NAME=w_lagdays VALUE="'
		. $lagdays
		. '"</TD>'
		. '</TR>'
		. '<TR><TD></TD><TD></TD></TR>'

	;

	if ($mc != '*new') {
		
		echo '<TR><TH ALIGN=CENTER>Day No</TH><TH ALIGN=LEFT>Influence<BR>(1-100%)</TH></TR>';
				
		for ($i = 0; $i <= $lagdays; ++$i)	{
			$sql = 'SELECT marketingchannellagid, influence'
				. '	FROM vs_marketingchannellag'
				. ' WHERE marketingchannelcd = "'
				. $marketingchannelcd
				. '"'
				. ' AND dayno = '
				. $i
			;
		
			$result = $conn->query($sql);
			$row = $result->fetch_row();
			if ($result->num_rows > 0) {
				$influence = $row[1];
			} else {
				$influence = 0;
			}

			echo 
				'<TR>'
				. '<TD ALIGN=CENTER>'
				. $i
				. '</TD>'
				. '<TD>'
				. '<INPUT TYPE=TEXT SIZE=3 NAME=influence'
				. $i
				. ' VALUE='
				. $influence
				. ' PATTERN="\d*">'
				. '<INPUT TYPE=HIDDEN NAME=w_influence'
				. $i
				. ' VALUE='
				. $influence
					. '</TD>'
				. '</TR>'
			;
			if ($result->num_rows > 0) {
				echo 
					'<INPUT TYPE=HIDDEN NAME=marketingchannellagid'
					. $i
					. " VALUE="
					. $row[0]
					. ">";
			}
		}
	}

  	
	$conn->close();
	
	echo "</TABLE>";
	echo "<INPUT TYPE=SUBMIT VALUE=Update>";
	echo "</FORM>";
	

?>


<A HREF=MarketingChannels.php>Return</A>


</HTML>