<HTML>

<?php
	include('header.html');
?>

<?php

	parse_str($_SERVER["QUERY_STRING"]);

	require('getVisitorId.php');
	require('logConversionEvent.php');
	
	$vs_visitorId = getVisitorId();
	
	$conversionId = rand(10000, 99999);
	$conversionValue = rand(50, 150);

	logConversionEvent($vs_visitorId, $conversionId, $conversionValue);
	
?>


</HTML>